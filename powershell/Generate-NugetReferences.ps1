param(
  [Parameter(Mandatory = $true)]
  [string] $SolutionFolderPath,
  [Parameter(Mandatory = $true)]
  [string] $PackagesConfigPath
)

$ErrorActionPreference = "Stop"

if (!(Test-Path $SolutionFolderPath)) {
  throw "Could not find path: $SolutionFolderPath"
}

if (!(Test-Path $PackagesConfigPath)) {
  throw "Could not find path: $PackagesConfigPath"
}

if (!(Test-Path "$SolutionFolderPath\.yo-rc.json")) {
  throw "Could not find path: $SolutionFolderPath\.yo-rc.json"
}

Add-Type -Assembly Microsoft.VisualBasic


try {
  Push-Location $SolutionFolderPath

  $sourceNugetExe = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
  $targetNugetExe = ".\nuget.exe"

  if (!(Test-Path $targetNugetExe)) {
    Invoke-WebRequest $sourceNugetExe -OutFile $targetNugetExe
  }

  Write-Host "Installing packages from $PackagesConfigPath" -ForegroundColor Cyan
  #& nuget.exe install $PackagesConfigPath -OutputDirectory "packages" -ConfigFile nuget.config


  $yorc = (Get-Content ".\.yo-rc.json" | Out-String | ConvertFrom-Json)
  $yorc = $yorc."generator-helixbase"
  $dotNetVersion = $yorc.dotNetVersion.Replace(".", "")
  $assemblyFolder = $yorc.assemblyFolder

  if ($dotNetVersion.Length -le 2) {
    $dotNetVersion = "$($dotNetVersion)0"
  }

  $projectDir = [IO.Directory]::GetParent($PackagesConfigPath)
  $projectFile = (Get-ChildItem $projectDir *.csproj).FullName

  $projectFileXml = New-Object System.Xml.XmlDataDocument
  $projectFileXml.Load($projectFile)

  $configFile = New-Object System.Xml.XmlDataDocument
  $configFile.Load($PackagesConfigPath)

  # Looping through packages in packages.config
  foreach ($node in $configFile.DocumentElement.ChildNodes) {
    $packageName = $node.GetAttribute("id")
    $packageVersion = $node.GetAttribute("version")
    $packageFolder = "$packageName.$packageVersion"
    $packageLibFolder = ".\packages\$packageFolder\lib"

    if (!(Test-Path $packageLibFolder)) {
      Write-Host "Could not find path: $packageLibFolder" -ForegroundColor Yellow
      continue
    }

    # Should contain all the .NET versions
    $versions = New-Object System.Collections.ArrayList($null)
    $versionPath = $null

    # Looping through directories in package's lib folder
    foreach ($directory in Get-ChildItem $packageLibFolder -Directory) {
      $dotNetFolderName = $directory.Name

      # $assemblyFolder could be for example 'netstandard2.0'
      if ($dotNetFolderName -eq $assemblyFolder) {
        $versionPath = $directory.FullName
        break
      }

      if ($dotNetFolderName.StartsWith("net")) {
        $packageDotNetVersion = $dotNetFolderName.Substring(3) # Removed 'net'

        # Adds a zero to be the same length
        if ($packageDotNetVersion.Length -le 2) {
          $packageDotNetVersion = "$($packageDotNetVersion)0"
        }

        if ([Microsoft.VisualBasic.Information]::IsNumeric($packageDotNetVersion)) {
          # Add the version to array
          $versions.Add($packageDotNetVersion) | Out-Null
        }
      }
    }

    $closestVersion = $null
    if ($null -eq $versionPath) {
      
      foreach ($foundVersion in $versions) {
        
        if ([int]$foundVersion -lt [int]$dotNetVersion) {
          $closestVersion = $foundVersion
        }

        if ($foundVersion -eq $dotNetVersion) {
          $closestVersion = $foundVersion
          break
        }

        if ([int]$foundVersion -gt [int]$dotNetVersion) {
          break
        }
      }
    }

    if ($null -ne $closestVersion -or $null -ne $versionPath) {
      if ($null -eq $versionPath) {
        $versionPath = "$packageLibFolder\net$closestVersion"
        if (!(Test-Path $versionPath)) {
          $versionPath = $versionPath.Substring(0, $versionPath.Length - 1)
        }
      }
      
      if (Test-Path $versionPath) {
        
        # Find the first <ItemGroup> element
        $itemGroup = $projectFileXml.DocumentElement.ChildNodes | Where-Object { $_.Name -eq "ItemGroup" } | Select-Object -First 1

        foreach ($dll in Get-ChildItem $versionPath *.dll) {
          $AssemblyPath = $dll.FullName
          $packagesIndex = $AssemblyPath.LastIndexOf("packages")
          $hintPath = "..\..\..\..\$($AssemblyPath.Substring($packagesIndex))"
          $fullName = ([System.Reflection.Assembly]::LoadFile($AssemblyPath)).FullName
          
          # Use nuget package name instead of dll name
          #$firstCommaIndex = $fullName.IndexOf(",")
          #$fullName = "$packageName$($fullName.Substring($firstCommaIndex))"

          [xml]$reference = @"
<Reference Include="$fullName, processorArchitecture=MSIL">
<HintPath>$hintPath</HintPath>
</Reference>
"@

          # Adding reference node to csproj file
          $itemGroup.AppendChild($projectFileXml.ImportNode($reference.DocumentElement, $true)) | Out-Null
          $projectFileXml.Save($projectFile)
          Write-Host "Added reference for $fullName" -ForegroundColor Green
        }
      } else {
        Write-Host "Could not find version folder for $versionPath"
      }
    } else {
      Write-Host "No matching versions found for $packageName" -ForegroundColor Yellow
    }
  }

  # Removing xml namespace as the .Save() method add them
  # https://stackoverflow.com/questions/10308598/xml-namespace-and-c-sharp-csproj
  $projectFileXml = [xml] $projectFileXml.OuterXml.Replace(" xmlns=`"`"", "")
  $projectFileXml.Save($projectFile)

  Remove-Item $targetNugetExe

  Pop-Location
}
catch {
  Pop-Location
  throw $_
}