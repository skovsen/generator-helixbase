/*eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
import colors = require("ansi-colors")
const { spawn } = require("child_process");

class PowerShell {

	runAsync(pathToScriptFile, parameters) {
		console.log(colors.cyan(`Powershell - running: ${pathToScriptFile} ${parameters}`));
		const child = spawn("powershell.exe", [ pathToScriptFile.replace(/(.+)/gi, "&(“$1”)"), parameters ]);

		child.stdout.setEncoding("utf8");
		child.stderr.setEncoding("utf8");

		child.stdout.on("data", (data) => {
			console.log(data);
		});

		child.stderr.on("data", (data) => {
			console.error(colors.red(`Error: ${data}`));
		});

		child.on("exit", () => {
			console.log(colors.cyan(`Powershell - done running ${pathToScriptFile} ${parameters}`));
		});

		child.stdin.end();
	}
}

export default new PowerShell()