const { execSync } = require("child_process")
import * as fs from "fs"
import * as path from "path"
import * as Generator from "yeoman-generator"
import colors = require("ansi-colors")

export class BaseGenerator<TSettings, TTemplateData> extends Generator {

  currentPath: string
  installPath: string
  settings: TSettings
  templateData: TTemplateData

  constructor(args: string|string[], opts: {}, currentPath: string, installPath: string) {
    super(args, opts)
    this.currentPath = currentPath
    this.installPath = installPath
  }

  _execute(command: string, workingDirectory: string = undefined): string {
    let response
    if (workingDirectory) {
      if (!fs.existsSync(workingDirectory)) {
        throw `Could not find working directory "execute" function: ${workingDirectory}`
      }
      this.log(colors.cyan(`Executing cmd ${command} on ${workingDirectory}`))
      response = execSync(command, { cwd: workingDirectory, encoding: "utf-8" })
    } else {
      this.log(colors.cyan(`Executing cmd ${command}`))
      response = execSync(command, { encoding: "utf-8" })
    }
    
    this.log(response)
    return response
  }

  _copyTemplateFolder(templateFolder: string, excludePaths: string[] = [], destinationPath: string = "") {
    this._copyTemplateFolderResursive(templateFolder, excludePaths, destinationPath, templateFolder)
  }
  
  private _copyTemplateFolderResursive(templateFolder: string, excludePaths: string[] = [], destinationPath: string = "", basePath: string = "") {
    const templateFolderPath = path.join(this.currentPath, "templates", templateFolder)

    fs.readdirSync(templateFolderPath)
      .forEach(file => {
        let filePath = path.join(templateFolder, file).replace(/\\/g, "/")
        filePath = filePath.startsWith("/") ? filePath.substring(1) : filePath

        if (!excludePaths.includes(filePath)) {
          if (file.indexOf(".") > -1) {
            const filename = file.startsWith("_") ? file.substring(1) : file
            let destination = path.join(templateFolder, filename).substring(basePath.length)
            destination = path.join(destinationPath, destination).replace(/\\/g, "/")
            destination = destination.startsWith("/") ? destination.substring(1) : destination
            this._copyTemplateFile(filePath, destination)
          } else {
            this._copyTemplateFolderResursive(path.join(templateFolder, file), excludePaths, destinationPath, basePath)
          }
        }
      })
	}

  _copyTemplateFile(template: string, destination: string) {
    this.log(destination)

		this.fs.copyTpl(
			this.templatePath(template),
			this.destinationPath(destination),
			this.templateData
		)
  }
}