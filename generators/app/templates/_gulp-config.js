module.exports = {
  webroot: "<%= webroot %>",
  defaultSite: "<%= localWebsiteUrl %>",
  solutionName: "<%= solutionName %>",
  vendorPrefix: "<%= vendorPrefix %>",
  defaultBuildConfiguration: "Debug",
  serializationLocation: ".\\App_Data\\Unicorn",
  msbuildToolsVersion: "auto",
  unitTestExecutable: ".\\node_modules\\nunit-console-runner\\packages\\NUnit.ConsoleRunner.3.9.0\\tools\\nunit3-console.exe"
};