const gulp = require("gulp")
const msbuild = require("gulp-msbuild")
const debug = require("gulp-debug")
const foreach = require("gulp-foreach")
const clean = require("gulp-clean")
const args = require("yargs").argv
const path = require("path")
const fs = require("fs")
const gulpConfig = require("./gulp-config.js")
const colors = require("ansi-colors")
const nunit = require("gulp-nunit-runner")
const { execSync } = require("child_process")

const buildConfiguration = args.configuration ? args.configuration : gulpConfig.defaultBuildConfiguration
const buildTargets = args.rebuild ? ["Clean", "Rebuild"] : ["Build"]
const site = args.site || gulpConfig.defaultSite
const webrootPath = gulpConfig.webroot && site ? path.resolve(gulpConfig.webroot, site) : undefined
const webrootRelativePath = gulpConfig.webroot && site ? path.join(gulpConfig.webroot, site) : undefined

function mkdirSyncRecursive(targetDir) {
  if (!fs.existsSync(targetDir)) {
    for (let i = targetDir.length-2; i >= 0; i--) {
      if (targetDir.charAt(i) == '/' || targetDir.charAt(i) == path.sep) {
        mkdirSyncRecursive(targetDir.slice(0, i));
        break;
      }
    }
    try {
      fs.mkdirSync(targetDir);
    } catch (err) {
      if (err.code !== 'EEXIST') 
        throw err;
    }
  }
}

function execute(command, workingDirectory) {
  let response
  if (workingDirectory) {
    if (!fs.existsSync(workingDirectory)) {
      throw `Could not find working directory "execute" function: ${workingDirectory}`
    }
    console.log(colors.cyan(`Executing cmd ${command} on ${workingDirectory}`))
    response = execSync(command, { cwd: workingDirectory, encoding: "utf-8" })
  } else {
    console.log(colors.cyan(`Executing cmd ${command}`))
    response = execSync(command, { encoding: "utf-8" })
  }
  
  console.log(response)
}

function cleanProjectFiles(layerName) {
  const filesToDelete = [
    `${webrootPath}\\bin\\${gulpConfig.vendorPrefix}.${layerName}.*`,
    `${webrootPath}\\App_Config\\Include\\${layerName}`
  ]

  console.log(colors.cyan(`Removing ${layerName} configs/binaries`))

  return gulp.src(filesToDelete, { read: false, allowEmpty: true })
    .pipe(clean({ force: true }))
}

function publishProjects(locations) {
  console.log(colors.cyan(`Publishing ${locations} to ${webrootPath}`))
  
  return gulp.src(locations)
    .pipe(foreach((stream) => 
      stream
        .pipe(debug({ title: colors.cyan("Building project:") }))
        .pipe(msbuild({
          targets: buildTargets,
          configuration: buildConfiguration,
          logCommand: false,
          verbosity: "minimal",
          stdout: true,
          errorOnFail: true,
          maxcpucount: 0,
          toolsVersion: gulpConfig.msbuildToolsVersion,
          customArgs: ["/warnaserror"],
          properties: {
            DeployOnBuild: "true",
            DeployDefaultTarget: "WebPublish",
            WebPublishMethod: "FileSystem",
            DeleteExistingFiles: "false",
            publishUrl: webrootPath,
            WarningLevel: 0,
            _FindDependencies: "false"
          }
        }))
    ))
}

function publishHelixProjects(location) {
  if(!webrootPath || !fs.existsSync(webrootPath)) {
    mkdirSyncRecursive(webrootPath);
  }
  
  const files = `./src/${location}/**/code/*.csproj`
  console.log(colors.cyan(`Publishing ${location} to ${webrootPath}`))

  if (args.includeSerialization) {
    const serializationFiles = `./src/${location}/**/serialization/**/*.yml`
    const destination = path.join(webrootRelativePath, gulpConfig.serializationLocation, location)

    console.log(colors.cyan(`Publishing serialization items: "${serializationFiles}" to "${destination}"`))
    gulp.src(serializationFiles)
      .pipe(gulp.dest(destination))
  }

  return publishProjects(files)
}

gulp.task("NUnit", () => {
  if(!gulpConfig.unitTestExecutable || !fs.existsSync(gulpConfig.unitTestExecutable)) {
    console.log(colors.red("Could not find path: " + gulpConfig.unitTestExecutable))
    return
  }

  return gulp.src(["./src/**/bin/**/*.Tests.dll"], {read: false})
    .pipe(nunit({
      executable: path.resolve(gulpConfig.unitTestExecutable)
    }))
})

gulp.task("Build-Solution", () => {
  console.log(colors.cyan(`Build configuration: ${buildConfiguration}`))

  return gulp.src(`./${gulpConfig.solutionName}.sln`)
    //.pipe(debug({ title: colors.cyan("NuGet restore:") }))
    //.pipe(nugetRestore())
    .pipe(debug({ title: colors.cyan("Building solution:") }))
    .pipe(msbuild({
      customArgs: ["/warnaserror"],
      targets: buildTargets,
      configuration: buildConfiguration,
      logCommand: false,
      verbosity: "minimal",
      stdout: true,
      errorOnFail: true,
      maxcpucount: 0,
      toolsVersion: gulpConfig.msbuildToolsVersion
    }))
})

// gulp.task("Serialize-Items", (done) => {
//   if (!frontendFolder) {
//     throw "Could not determine frontend folder. Have you added argument --app?"
//   }

//   execute("npm run deploy-items-local", frontendFolder)
  
//   const unicornSyncPath = path.join(webrootPath, "App_Data\\Tools\\PowerShell\\Unicorn")
//   const unicornSyncFilePath = path.join(unicornSyncPath, "Unicorn-Sync.ps1")
//   const siteSettingsFile = path.join(webrootPath, "App_Data\\site-settings.json")
  
//   try {
//     if (fs.existsSync(unicornSyncFilePath) && fs.existsSync(siteSettingsFile)) {
//       const siteSettings = require(siteSettingsFile)
//       const secret = siteSettings.sitecore.unicorn.sharedSecret
//       const controlPanelUrl = `http://${site}${siteSettings.sitecore.unicorn.controlPanelUrl}`

//       execute(`PowerShell.exe .\\Unicorn-Sync.ps1 -secret ${secret} -controlPanelUrl ${controlPanelUrl}`, unicornSyncPath)
//     } else {
//       console.log(colors.red(`Could not find file: ${unicornSyncFilePath}`))
//       console.log(colors.red(`Make sure you have site-settings.json file, with unicorn secret in it: ${siteSettingsFile}`))  
//     }
//   }
//   catch(e) {
//     console.log(colors.red(`Make sure you have site-settings.json file, with unicorn secret in it: ${siteSettingsFile}`))
//     console.log(colors.red("Try to run 'npm run publish' and try again"))

//     console.log(colors.red(e))
//   }
  
//   done()
// })

gulp.task("Publish-Foundation-Layer", () => {
  return cleanProjectFiles("Foundation"),
    publishHelixProjects("Foundation")
})

gulp.task("Publish-Feature-Layer", () => {
  return cleanProjectFiles("Feature"),
    publishHelixProjects("Feature")
})

gulp.task("Publish-Project-Layer", () => {
  return cleanProjectFiles("Project"),
    publishHelixProjects("Project")
})

gulp.task("Publish-All-Projects",
  gulp.series(
    "Build-Solution",
    "Publish-Foundation-Layer",
    "Publish-Feature-Layer",
    "Publish-Project-Layer"
  )
)

gulp.task("Test",
  gulp.series(
    "Build-Solution",
    "NUnit"
  )
)