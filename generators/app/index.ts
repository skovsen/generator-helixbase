import * as fs from "fs"
import * as path from "path"
import * as Generator from "yeoman-generator"
import { BaseGenerator } from "../../utils/BaseGenerator"
import guid = require("uuid")
import yosay = require("yosay")

const installPath = process.cwd()

interface Settings {
  solutionName?: string
  localWebsiteUrl?: string
  dotNetVersion?: string
  vendorPrefix?: string
  commonProjectName?: string
  webroot?: string
  packageReference?: boolean
}

interface TemplateData extends Settings {
  commonProjectGuid?: string
  commonProjectFolderGuid?: string
  projectGuid?: string
  featureGuid?: string
  foundationGuid?: string
}

module.exports = class extends BaseGenerator<Settings, TemplateData> {
  
  constructor(args: string|string[], opts: {}) {
		super(args, opts, __dirname, installPath)
    this.argument("solutionName", { type: String, required: false })
    this.argument("vendorPrefix", { type: String, required: false })
    this.settings = {}
    this.templateData = {}
  }

  async askForSolutionName() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "solutionName",
      message: "Your project name",
      when: !this.options.solutionName
    }]

    const answers = await this.prompt(questions)

    this.settings.solutionName = answers.solutionName || this.options.solutionName
    this._validateSolutionName(this.settings.solutionName)
  }

  async askForVendorPrefix() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "vendorPrefix",
      message: "Your project's vendor prefix",
      default: this.settings.solutionName,
      when: !this.options.solutionName
    }]

    const answers = await this.prompt(questions)

    this.settings.vendorPrefix = answers.vendorPrefix || this.options.vendorPrefix
    this._validateSolutionName(this.settings.vendorPrefix)

    this.settings.commonProjectName = `${this.settings.vendorPrefix}.Project.Common`;
  }

  async askForLocalWebsiteUrl() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "localWebsiteUrl",
      message: "Your project's local website url",
      default: `dev-${this.settings.vendorPrefix}.com`.toLowerCase()
    }]

    const answers = await this.prompt(questions)

    this.settings.localWebsiteUrl = answers.localWebsiteUrl
  }

  async askForWebroot() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "webroot",
      message: "Your project's webroot folder",
      default: "www"
    }]

    const answers = await this.prompt(questions)

    this.settings.webroot = answers.webroot
  }

  async askForDotNetVersion() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "dotNetVersion",
      message: ".NET Framework version used", 
      default: "4.7.1",
      choices: [{
        name: "4.7.1",
        value: "4.7.1",
      }, {
        name: "4.7.2",
        value: "4.7.2",
      }]
    }]
    
    const answers = await this.prompt(questions)
    
    this.settings.dotNetVersion = answers.dotNetVersion
  }

  async askForPackageReference() {
    const questions: Generator.Questions = [{
      type: "confirm",
      name: "packageReference",
      message: "Would you like to use package reference instead of packages.config?",
      default: true
    }]

    const answers = await this.prompt(questions)
    this.settings.packageReference = answers.packageReference
  }
  
  private _validateSolutionName(solutionName: string) {
    if (solutionName.indexOf("/") > -1) {
      throw new Error("SolutionName must not contain '/'")
    }
  }

  init() {
    this.log(yosay("Welcome to the Helixbase generator!"))
    for (let settingProp in this.settings) {
      this.log(`${settingProp}: ${this.settings[settingProp]}`)
    }
    this._buildTemplateData();
  }

  copySolutionFiles() {
    this._copyTemplateFolder("/", [
      "_solution.sln",
      "src/Project/Common/code/_ProjectName.csproj",
      "src/Project/Common/code/_packages.config"
    ]);
    this._copyTemplateFile("_solution.sln", this.settings.solutionName + ".sln")
    this._copyTemplateFile("src/Project/Common/code/_ProjectName.csproj", `src/Project/Common/code/${this.settings.commonProjectName}.csproj`)
    if (!this.settings.packageReference) {
      this._copyTemplateFile("src/Project/Common/code/_packages.config", "src/Project/Common/code/packages.config")
    }
  }

  end() {
    const projectPath = path.join(installPath, this.settings.solutionName)
    this._execute("npm install", projectPath)
    this._execute("npm run rebuild", projectPath)
  }

  private _buildTemplateData() {
    this.templateData.commonProjectGuid = guid.v4();
		this.templateData.commonProjectFolderGuid = guid.v4();
		this.templateData.projectGuid = guid.v4();
		this.templateData.featureGuid = guid.v4();
		this.templateData.foundationGuid = guid.v4();
		
    for (let settingProp in this.settings) {
      this.templateData[settingProp] = this.settings[settingProp]
    }
  }
  
  _copyTemplateFile(template, destination) {
    destination = path.join(this.settings.solutionName, destination)
    super._copyTemplateFile(template, destination)
  }
}