import * as fs from "fs"
import * as path from "path"
import * as Generator from "yeoman-generator"
import { BaseGenerator } from "../../utils/BaseGenerator"
import guid = require("uuid")
import yosay = require("yosay")
import powershell from "../../utils/Powershell"

const installPath = process.cwd()

interface NugetDependency {
  name: string
  version: string
}

interface Settings {
  solutionName?: string
  sourceFolder?: string
  dotNetVersion?: string
  vendorPrefix?: string
  layer?: string
  module?: string
  projectName?: string
  projectPath?: string
  serialization?: boolean
  registerContainer?: boolean
  packageReference?: boolean
  automaticSolutionInsertion?: boolean,
  defaultProjectDependencies?: NugetDependency[]
}

interface TemplateData extends Settings {
  projectGuid?: string
  projectFolderGuid?: string
}

module.exports = class extends BaseGenerator<Settings, TemplateData> {
  
  constructor(args: string|string[], opts: {}) {
		super(args, opts, __dirname, installPath)
    this.argument("layer", { type: String, required: false })
    this.argument("module", { type: String, required: false })
    this.settings = this.config.getAll()
    this.templateData = {}
  }

  async askForLayer() {
    const questions: Generator.Questions = [{
      type: "list",
      name: "layer",
      message: "What layer do you want to add the project too?",
      when: !this.options.layer,
      choices: [
				{
					name: "Feature layer?",
					value: "Feature"
				}, {
					name: "Foundation layer?",
					value: "Foundation"
				}, {
					name: "Project layer?",
					value: "Project"
				},
			],
    }]

    const answers = await this.prompt(questions)
    this.settings.layer = answers.layer || this.options.layer
  }

  async askForModule() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "module",
      message: "Module name:",
      when: !this.options.module
    }]

    const answers = await this.prompt(questions)

    this.settings.module = answers.module || this.options.module
    this.settings.projectName = `${this.settings.vendorPrefix}.${this.settings.layer}.${this.settings.module}`;
  }

  async askForUnicorn() {
    const questions: Generator.Questions = [{
      type: "confirm",
      name: "serialization",
      message: "Would you like to include Unicorn (serialization)?",
      default: true
    }]

    const answers = await this.prompt(questions)
    this.settings.serialization = answers.serialization
  }

  async askForRegisterContainer() {
    const questions: Generator.Questions = [{
      type: "confirm",
      name: "registerContainer",
      message: "Would you like to register a DI container?",
      default: true
    }]

    const answers = await this.prompt(questions)
    this.settings.registerContainer = answers.registerContainer
  }

  async askForPackageReference() {
    const questions: Generator.Questions = [{
      type: "confirm",
      name: "packageReference",
      message: "Would you like to use package reference instead of packages.config?",
      default: true,
      when: typeof this.settings.packageReference === "undefined"
    }]

    const answers = await this.prompt(questions)
    this.settings.packageReference = answers.packageReference || this.settings.packageReference
    this.config.set("packageReference", this.settings.packageReference)
  }

  async askForVendorPrefix() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "vendorPrefix",
      message: "Project's vendor prefix",
      when: !this.settings.vendorPrefix,
      default: this.settings.solutionName
    }]

    const answers = await this.prompt(questions)
    this.settings.vendorPrefix = answers.vendorPrefix || this.settings.vendorPrefix
    this.config.set("vendorPrefix", this.settings.vendorPrefix)
  }

  async askForSolutionName() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "solutionName",
      message: "Name of the solution file",
      when: !this.settings.solutionName,
      default: this.settings.vendorPrefix
    }]

    const answers = await this.prompt(questions)
    this.settings.solutionName = answers.solutionName || this.settings.solutionName
    this.config.set("solutionName", this.settings.solutionName)
  }

  async askForSourceFolder() {
    const questions: Generator.Questions = [{
      type: "input",
      name: "sourceFolder",
      message: "Path to source folder",
      when: !this.settings.sourceFolder,
      default: "src"
    }]

    const answers = await this.prompt(questions)
    this.settings.sourceFolder = answers.sourceFolder || this.settings.sourceFolder
    this.config.set("sourceFolder", this.settings.sourceFolder)
  }

  async askForAutomaticSolutionInsertion() {
    const questions: Generator.Questions = [{
      type: "confirm",
      name: "automaticSolutionInsertion",
      message: "Automatically add project to solution file?",
      default: true
    }]

    const answers = await this.prompt(questions)
    this.settings.automaticSolutionInsertion = answers.automaticSolutionInsertion
  }

  init() {
    this.log(yosay("Let's generate that project!"));
    for (let settingProp in this.settings) {
      this.log(`${settingProp}: ${this.settings[settingProp]}`)
    }
    this._buildTemplateData();
  }

  copyProjectFiles() {
    const modulePath = `${this.settings.sourceFolder}/${this.settings.layer}/${this.settings.module}`
    this._copyTemplateFolder("ProjectName", [
      "ProjectName/code/_ProjectName.csproj",
      "ProjectName/code/App_Config/Include/_RegisterContainer.config",
      "ProjectName/code/App_Config/Include/_Serialization.config",
      "ProjectName/code/DI/_RegisterContainer.cs"
    ], modulePath);

    this.settings.projectPath = `${modulePath}/code`
    this._copyTemplateFile("ProjectName/code/_ProjectName.csproj", `${this.settings.projectPath}/${this.settings.projectName}.csproj`)
  }

  copySerializationConfig() {
    if (this.settings.serialization) {
      this._copyTemplateFile("ProjectName/code/App_Config/Include/_Serialization.config", 
                             `${this.settings.projectPath}/App_Config/Include/${this.settings.layer}/${this.settings.module}.Serialization.config`)
    }
  }

  copyContainerFiles() {
    if (this.settings.registerContainer) {
      this._copyTemplateFile("ProjectName/code/App_Config/Include/_RegisterContainer.config", 
                             `${this.settings.projectPath}/App_Config/Include/${this.settings.layer}/${this.settings.module}.RegisterContainer.config`)
      
      this._copyTemplateFile("ProjectName/code/DI/_RegisterContainer.cs", `${this.settings.projectPath}/DI/RegisterContainer.cs`)
    }
  }

  

  addToSolutionFile() {
    if (this.settings.automaticSolutionInsertion) {
      const files = fs.readdirSync(this.destinationPath())
      const solutionFile = files.find(file => file.toLowerCase().endsWith(".sln"))
      
      const scriptParameters = `-SolutionFile '${this.destinationPath(solutionFile)}' ` +
                               `-Name '${this.settings.projectName}' ` +
                               `-Type '${this.settings.layer}' ` +
                               `-ProjectPath '${this.settings.projectPath}' ` +
                               `-SolutionFolderName '${this.settings.module}'`

      const pathToAddProjectScript = path.join(__dirname, "../../powershell/add-project.ps1")
      
      this.log(pathToAddProjectScript)

      powershell.runAsync(pathToAddProjectScript, scriptParameters)
    }
  }

  end() {
    this._installProjectDependencies()
  }

  private _installProjectDependencies() {
    for (let dependency of this.settings.defaultProjectDependencies) {
      this.log(dependency.name, dependency.version)
    }

    const solutionFile = path.join(this.installPath, this.settings.solutionName + ".sln")
    const packageConfigPath = path.join(this.installPath, this.settings.projectPath, "packages.config")

    this._execute(`nuget restore ${solutionFile}`, this.installPath)

    const pathToGenerateNugetReferences = path.join(__dirname, "../../powershell/Generate-NugetReferences.ps1")

    const result = this._execute(`Powershell.exe ${pathToGenerateNugetReferences} -SolutionFolderPath '${this.installPath}' -PackagesConfigPath '${packageConfigPath}'`)
  }
  
  private _buildTemplateData() {
    this.templateData.projectGuid = guid.v4();
    this.templateData.projectFolderGuid = guid.v4();
    
    for (let settingProp in this.settings) {
      this.templateData[settingProp] = this.settings[settingProp]
    }
  }
}